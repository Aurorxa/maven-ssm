package com.xudaxian.service;

import com.github.pagehelper.PageInfo;
import com.xudaxian.domain.User;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-14 10:47
 */
public interface UserService {

    /**
     * 保存用户信息
     *
     * @param user 用户信息
     * @return
     */
    boolean save(User user);

    /**
     * 更新用户信息
     *
     * @param user 用户信息
     * @return
     */
    boolean update(User user);

    /**
     * 删除用户信息
     *
     * @param uuid 主键
     * @return
     */
    boolean delete(String uuid);

    /**
     * 根据主键获取用户信息
     *
     * @param uuid 主键
     * @return
     */
    User get(String uuid);

    /**
     * 分页显示用户信息
     *
     * @param page 页码
     * @param size 每页显示条数
     * @return
     */
    PageInfo<User> getAll(int page, int size);

    /**
     * 根据用户名和密码登录
     *
     * @param userName
     * @param password
     * @return
     */
    User login(String userName, String password);
}
