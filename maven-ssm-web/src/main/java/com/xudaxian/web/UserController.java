package com.xudaxian.web;

import com.github.pagehelper.PageInfo;
import com.xudaxian.domain.User;
import com.xudaxian.service.UserService;
import com.xudaxian.utils.Code;
import com.xudaxian.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-14 12:40
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping
    public Result save(User user) {
        boolean flag = userService.save(user);
        return new Result(flag ? Code.SAVE_OK : Code.SAVE_ERROR);
    }

    @PutMapping
    public Result update(User user) {
        boolean flag = userService.update(user);
        return new Result(flag ? Code.UPDATE_OK : Code.UPDATE_ERROR);
    }

    @DeleteMapping("/{uuid}")
    public Result delete(@PathVariable String uuid) {
        boolean flag = userService.delete(uuid);
        return new Result(flag ? Code.DELETE_OK : Code.DELETE_ERROR);
    }

    @GetMapping("/{uuid}")
    public Result get(@PathVariable String uuid) {
        User user = userService.get(uuid);
        return new Result(null != user ? Code.GET_OK : Code.GET_ERROR, user);
    }

    @GetMapping("/{page}/{size}")
    public Result getAll(@PathVariable Integer page, @PathVariable Integer size) {
        PageInfo<User> all = userService.getAll(page, size);
        return new Result(null != all ? Code.GET_OK : Code.GET_ERROR, all);
    }

    @PostMapping("/login")
    public Result login(String userName, String password) {
        User user = userService.login(userName, password);
        return new Result(null != user ? Code.GET_OK : Code.GET_ERROR, user);
    }


}
